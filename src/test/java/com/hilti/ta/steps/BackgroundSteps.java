package com.hilti.ta.steps;

import java.util.Date;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;


import com.hilti.ta.utils.WebDriverFactory;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;

/**
 * Cucumber hook steps definition class responsible for actions taken before and after test execution.
 */
public class BackgroundSteps {

	@Before
	public void beforeUITests() {
		WebDriverFactory.initialize();
	}
	
	@Before // io.cucumber.java.Before
	public void logStartTime(Scenario scenario){
	    scenario.write("Start time: " + new Date()); 
	    scenario.write( scenario.getName());
	    
	   
	}

	@After // io.cucumber.java.Before
	public void logEndTime(Scenario sce){
	    sce.write("End time: " + new Date());    
	}
	
	@After
	public void afterUITests(Scenario scenario) {
		if (!scenario.isFailed()) {
			WebDriverFactory.quitCurrentDriver();
		}
		WebDriverFactory.quitCurrentDriver();
	}
	
	@AfterStep
	public void afterTestStep(Scenario scenario) {
		
		byte[] screenshot = ((TakesScreenshot)WebDriverFactory.getDriver()).getScreenshotAs(OutputType.BYTES);
        scenario.embed(screenshot, "image/png");
		
		
	}
}
