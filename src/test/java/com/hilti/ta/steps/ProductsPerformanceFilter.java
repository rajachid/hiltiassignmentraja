package com.hilti.ta.steps;

import com.hilti.ta.pages.CategoryPage;
import com.hilti.ta.pages.Homepage;
import com.hilti.ta.pages.components.ConsentsComponent;
import com.hilti.ta.pages.components.HeaderComponent;
import com.hilti.ta.services.BannersService;
import com.hilti.ta.services.BrowserService;
import com.hilti.ta.services.CookieService;
import com.hilti.ta.utils.Country;

import com.hilti.ta.utils.Products;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

/**
 * Cucumber steps definition class related to difference performance level
 * functionality.
 */
public class ProductsPerformanceFilter {

	private final CategoryPage categoryPage = new CategoryPage();

	private final Homepage homepage = new Homepage();

	@When("User navigates to category page for {} and {}")
	public void userNavigatesToProduct(final Country country, final Products product) {
		homepage.navigateToProduct(country, product);

	}

	@Then("User can see different performance level products on category page")
	public void checkOneInEachPerformanceLevelisPresent() {
		categoryPage.checkOneInEachPerformanceLevelisPresent();

	}

	@Then("User can see only {} performance level products on category page")
	public void checkOnlySelectedPerformanceLevelsPresentOnPage(String strPerformanceLevel) {
		categoryPage.checkOnlySelectedPerformanceLevelsPresentOnPage(strPerformanceLevel);

	}

	@When("User selects performance level {} in the filters")
	public void selectPerformanceLevelInTheFilter(String perf) {
		categoryPage.selectPerformanceLevelInTheFilter(perf);

	}

	@When("User resets the performance level filter")
	public void userResetsThePerformanceLevelFilter() {
		categoryPage.userResetsThePerformanceLevelFilter();

	}

}
