package com.hilti.ta.pages;

import java.io.BufferedReader;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.hilti.ta.utils.WebDriverFactory;
import io.cucumber.java.Scenario;

public abstract class PageObject {

	public static Scenario scenario;

	protected boolean isElementVisible(final By locator) {
		try {
			final WebElement consentBanner = WebDriverFactory.getWebDriverWait(2)
					.until(ExpectedConditions.presenceOfElementLocated(locator));
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			// WebDriverFactory.getWebDriverWait(2).until(ExpectedConditions.elementToBeClickable(locator)).click();;
			return consentBanner.isDisplayed();

		} catch (final TimeoutException e) {
			// Element not found
			return false;
		}
	}

	public void click(By ele) {

		WebDriverFactory.getDriver().findElement(ele).click();
	}

	public void clickByJS(WebElement ele) {
		try {
			JavascriptExecutor js = (JavascriptExecutor) WebDriverFactory.getDriver();
			js.executeScript("arguments[0].click();", ele);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void waitForLoadingIconInvisible() {
		
		WebDriverWait wait = new WebDriverWait(WebDriverFactory.getDriver(), 15);
		wait.until(ExpectedConditions.invisibilityOfElementLocated(By
				.xpath("//*[contains(@class,'preloader') and contains(@class,'active') and contains(@class,'show')]")));
		
	}

}
