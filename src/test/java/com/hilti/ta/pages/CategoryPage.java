package com.hilti.ta.pages;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import com.hilti.ta.utils.WebDriverFactory;
import com.hilti.ta.utils.Indicators;

/**
 * Page Object Model representing Product Category Page.
 */
enum indicatorTypes{STANDARD, PREMIUM , ULTIMATE}
public class CategoryPage extends PageObject {
	
	private static By productFilter;
	private static final By indicator = By
			.xpath("//descendant::*[contains(@class,'order-3')]//span[contains(@class,'indicator-label-title')]");
	private static final By gridItems = By.xpath("//*[contains(@class,'grid-item')]");
	private static final By noPreferenceFilter = By.xpath(
			"//button[contains(@class,'custom-radio')]//descendant::*[contains(text(),'No preference') or contains(text(),'Keine Präferenz') or contains(text(),'Sans préférence')]");

	public CategoryPage() {
		
	}

	public CategoryPage(String strFilterType) {
		productFilter = By
				.xpath("//button[contains(@class,'custom-radio')]//descendant::*[contains(text(),\"" + strFilterType + "\")]");
	}

	public void checkOnlySelectedPerformanceLevelsPresentOnPage(String strPerformanceLevel) {
		
		waitForLoadingIconInvisible();
		List<String> strActualCategoryTypes = new ArrayList<String>();
		List<WebElement> indicatorsinPage = WebDriverFactory.getDriver().findElements(indicator);
		indicatorsinPage.stream().forEach(x -> strActualCategoryTypes.add(x.getText()));
		Boolean b = (strActualCategoryTypes.stream().distinct().count() == 1)&& (strActualCategoryTypes.get(0).equals(strPerformanceLevel.toUpperCase()));
		Assert.assertTrue(b, String.format("Filter type " + strPerformanceLevel + " is not working as expected"));

	}

	public void checkOneInEachPerformanceLevelisPresent() {
		
		waitForLoadingIconInvisible();
		List<String> strActualCategoryTypes = new ArrayList<String>();
		List<WebElement> indicatorsinPage = WebDriverFactory.getDriver().findElements(indicator);
		indicatorsinPage.stream().forEach(x -> strActualCategoryTypes.add(x.getText()));
		Boolean b = strActualCategoryTypes.stream().distinct().collect(Collectors.toList()).containsAll(Arrays.asList(indicatorTypes.ULTIMATE.toString(), indicatorTypes.PREMIUM.toString(), indicatorTypes.STANDARD.toString()));
		Assert.assertTrue(b, String.format("Not all category types of STANDARD ULTIMATE AND PREMIUM is present"));
	}

	public void selectPerformanceLevelInTheFilter(String strPerformanceLevel) {

		By filterBy = new CategoryPage(strPerformanceLevel).productFilter;
		WebElement filter = WebDriverFactory.getDriver().findElement(filterBy);
		clickByJS(filter);
	}

	public void userResetsThePerformanceLevelFilter() {

		WebElement filter = WebDriverFactory.getDriver().findElement(noPreferenceFilter);
		clickByJS(filter);
	}

}
