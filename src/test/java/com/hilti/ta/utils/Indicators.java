package com.hilti.ta.utils;

import java.util.Locale;


public enum Indicators {
	ST("STANDARD"),
	UL("ULTIMATE"),
	PR("PREMIUM"); 
	

	private String IndicatorType;


	Indicators(final String IndicatorType) {
		this.IndicatorType = IndicatorType;
		
	
	}

	public String getIndicator() {
		return IndicatorType;
	}

	

	
}
