package com.hilti.ta.utils;

import java.util.Locale;

/**
 * Enumeration class for Locale and respective products page as per given requirement
 */
public enum Products {
	US("United States", "/c/CLS_DIAMOND_CUTTING_DISCS_7126"),
	DE("Germany", "/c/CLS_DIAMOND_GRINDING_WHEELS_7126"),
	FR("France", "/c/CLS_DIAMOND_GRINDING_WHEELS_7126"); 
	

	private String name;
	private String productCategory;
	private Locale defaultLocale;
	

	Products(final String name, final String domain) {
		this.name = name;
		this.productCategory = domain;
	
	}

	public String getName() {
		return name;
	}

	public String getProductCategory() {
		return productCategory;
	}

	
}
