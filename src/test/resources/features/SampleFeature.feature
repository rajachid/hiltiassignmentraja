@Smoke @Sample

Feature: Sample feature to demonstrate the capabilities of this project
 
  Scenario Outline: : Verify performance level filtering on category page for country <country>
    Given User opens Hilti website for country <country>
    When User navigates to category page for <country> and <URL>
    Then  User can see different performance level products on category page
    
    When User selects performance level <performance> in the filters
 		Then User can see only <performance> performance level products on category page
 		 
 		When User resets the performance level filter
 		Then User can see different performance level products on category page

    Examples:
      | country | URL    |performance |
      | US      | US     | Ultimate         |
       | DE      | DE     | Standard         |
       | FR      | FR     | Premium          |
     
     
 